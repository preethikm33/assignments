package one;
import java.util.*;

class Author{
	private String name;
	private String emailid;
	private char gender;

Author(String name, String emailid, char gender){
	this.name = name;
	this.emailid = emailid;
	this.gender = gender;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getEmailid() {
	return emailid;
}

public void setMailid(String mailid) {
	this.emailid = emailid;
}

public char getGender() {
	return gender;
}

public void setGender(char gender) {
	this.gender = gender;
}
}
class Book{
	private String name;
	private Author author;
	private double price;
	private int quantity;
Book(String name, Author author, double price,int quantity){
	this.name = name;
	this.author = author;
	this.price = price;
	this.quantity = quantity;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Author getAuthor() {
	return author;
}
public void setAuthor(Author author) {
	this.author = author;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public int getQuantity() {
	return quantity;
}
public void setQuantity(int quantity) {
	this.quantity = quantity;
}
public void displayAuthorDetails() {
	System.out.println("Displaying author details");
	System.out.println("Author name: " +getName());
	System.out.println("Author email: " +author.getEmailid());
	System.out.println("Author gender: " +author.getGender());
}

	
}
public class Tester5 {

	public static void main(String[] args) {
		Author author = new Author("Joshua Blouch", "Joshua@gmail.com",'M');
		Book book = new Book("Effective Java",author,45,15);
		book.displayAuthorDetails();

	}

}


	

